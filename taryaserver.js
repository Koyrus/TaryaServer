var express  = require('express');
var cors = require('cors');
var app = express();
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
app.use(cors());
var path = require("path");
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});




app.get("/" , function (req , res) {
    res.sendStatus(200)
});
//Login response after request from client side . Function check if credentials is true/false
app.post('/checkcredentials' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/checkcredentials",
        "headers": {
            "content-type": "application/json",
            "connection": "Keep-Alive",
            "user-agent": "Apache-HttpClient/4.1.1 (java 1.5)"
        }
    };
    var _req = http.request(options, function (_res , data ) {
        var chunks = [];
        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });
            _res.on("end", function () {
                var body = Buffer.concat(chunks);
                res.send(body);
                console.log(body.toString() , "Answer for client side");

            });
    });
    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken }));

    console.log(request.body,"----");
    _req.end();

    _req.on('error', function(err){
        console.log(JSON.stringify(err));
    });
});
// If username and password is correct , Node Api send request to Tarya Api with JSON data.
// After that Node Api send callback to client Side.
app.post('/searchrequests' ,function (request,res) {
    var loginData = {
        username: request.username,
        securityToken: request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/searchrequests",
        "headers": {
            "content-type": "application/json",
            "connection": "Keep-Alive",
            "user-agent": "Apache-HttpClient/4.1.1 (java 1.5)"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken
    }));
    _req.end();
});
////////////////////////////////////
app.post('/getonlinecs' ,function (request,res) {
    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var qs = require("querystring");
    var http = require("https");
    var LoginResponse = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/getonlinecs",
        "headers": {
            "content-type": "application/json",
        }
    };
    var _req = http.request(LoginResponse, function (_res , data ) {
        var chunks = [];
        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });
        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });
    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken ,
        personId: request.body.personId,
        email:  request.body.email,
        phone:  request.body.phone,
        identity: request.body.identity,
        ipAddress: request.body.ipAddress,
        countryCode : request.body.countryCode ,
        city:  request.body.city,
        street:  request.body.street,
        streetNumber:  request.body.streetNumber,
        zip_code:  request.body.zip_code,
        inputName:  request.body.inputName,
        date:  request.body.date,
        gender:  request.body.gender,
        marital_status:  request.body.marital_status,
        number_of_childrens:  request.body.number_of_childrens,
        socio_economic_index:  request.body.socio_economic_index,
        residence_status:  request.body.residence_status,
        house_value:  request.body.house_value,
        employment_status:  request.body.employment_status,
        employment_seniority:  request.body.employment_seniority,
        Household_income:  request.body.Household_income,
        monthly_income:  request.body.monthly_income,
        education:  request.body.education,
        value_of_vehicles:  request.body.value_of_vehicles,
        ownership_of_vehicles:  request.body.ownership_of_vehicles,
        ethnicity:  request.body.ethnicity,
        credit_limit_bank:  request.body.credit_limit_bank,
        credit_limit_credit_card:  request.body.credit_limit_credit_card,
        total_loans_in_relation_to_income:  request.body.total_loans_in_relation_to_income,
        total_expenditure_on_income:  request.body.total_expenditure_on_income,
        value_of_real_estate:  request.body.value_of_real_estate,
        total_monthly_load_payment:  request.body.total_monthly_load_payment,
        single_provider:  request.body.single_provider,
        pay_day:  request.body.pay_day,
        number_of_employes:  request.body.number_of_employes,
        credit_limit_usage_trend:  request.body.credit_limit_usage_trend,
        monthly_repayment_amount:  request.body.monthly_repayment_amount,
        mortgaged_assets_vehicles_real_estate:  request.body.mortgaged_assets_vehicles_real_estate,
        creditCardPaymentDay : request.body.creditCardPaymentDay,
        incomeMinusExpensesBenchmark : request.body.incomeMinusExpensesBenchmark
    }));
    console.log(request.body,"----");
    _req.end();
});
//////////////////////////
app.post('/getorgconfig' ,function (request,res) {
    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var qs = require("querystring");
    var http = require("https");
    var LoginResponse = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/getorgconfig",
        "headers": {
            "content-type": "application/json"
        }
    };
    var _req = http.request(LoginResponse, function (_res , data ) {
        var chunks = [];
        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });
        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });
    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken ,
        personId: request.body.personId,
        email:  request.body.email,
        phone:  request.body.phone,
        identity: request.body.identity,
        ipAddress: request.body.ipAddress,
        countryCode : request.body.countryCode ,
        city:  request.body.city,
        street:  request.body.street,
        streetNumber:  request.body.streetNumber,
        zip_code:  request.body.zip_code,
        inputName:  request.body.inputName,
        date:  request.body.date,
        gender:  request.body.gender,
        marital_status:  request.body.marital_status,
        number_of_childrens:  request.body.number_of_childrens,
        socio_economic_index:  request.body.socio_economic_index,
        residence_status:  request.body.residence_status,
        house_value:  request.body.house_value,
        employment_status:  request.body.employment_status,
        employment_seniority:  request.body.employment_seniority,
        Household_income:  request.body.Household_income,
        monthly_income:  request.body.monthly_income,
        education:  request.body.education,
        value_of_vehicles:  request.body.value_of_vehicles,
        ownership_of_vehicles:  request.body.ownership_of_vehicles,
        ethnicity:  request.body.ethnicity,
        credit_limit_bank:  request.body.credit_limit_bank,
        credit_limit_credit_card:  request.body.credit_limit_credit_card,
        total_loans_in_relation_to_income:  request.body.total_loans_in_relation_to_income,
        total_expenditure_on_income:  request.body.total_expenditure_on_income,
        value_of_real_estate:  request.body.value_of_real_estate,
        total_monthly_load_payment:  request.body.total_monthly_load_payment,
        single_provider:  request.body.single_provider,
        pay_day:  request.body.pay_day,
        number_of_employes:  request.body.number_of_employes,
        credit_limit_usage_trend:  request.body.credit_limit_usage_trend,
        monthly_repayment_amount:  request.body.monthly_repayment_amount,
        mortgaged_assets_vehicles_real_estate:  request.body.mortgaged_assets_vehicles_real_estate,
        creditCardPaymentDay : request.body.creditCardPaymentDay,
        incomeMinusExpensesBenchmark : request.body.incomeMinusExpensesBenchmark

    }));
    console.log(request.body,"----");
    _req.end();
});


app.post('/getorgcsfields' ,function (request,res) {
    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var qs = require("querystring");
    var http = require("https");
    var LoginResponse = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/getorgcsfields",
        "headers": {
            "content-type": "application/json"
        }
    };
    var _req = http.request(LoginResponse, function (_res , data ) {
        var chunks = [];
        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });
        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });
    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken
    }));

    console.log(request.body,"----");
    _req.end();
});




app.post('/searchbulkrequests' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/searchbulkrequests",
        "headers": {
            "content-type": "application/json",
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken ,
        pageNumber : request.body.pageNumber ,
        pageSize : request.body.pageSize,
        orderByField: 'date',
        orderDirection: 'desk',

    }));
    _req.end();
});

app.post('/ping' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/ping",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken
    }));
    _req.end();
});


app.post('/searchaccountusage' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/searchaccountusage",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken,
        pageNumber : request.body.pageNumber ,
        pageSize : request.body.pageSize,
        orderByField: 'date',
        orderDirection: 'desk',
        startDate: request.body.startDate,
        endDate: request.body.endDate
    }));
    _req.end();
});


app.post('/createhistoryfile' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/createhistoryfile",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken,
        bulkId: request.body.bulkId,
        records:
            [{
                date: request.body.date,
                bulkFileName: request.body.bulkFileName,
                personId: request.body.personId,
                email:  request.body.email,
                phone:  request.body.phone,
                identity: request.body.identity,
                ipAddress: request.body.ipAddress,
                countryCode : request.body.countryCode ,
                city:  request.body.city,
                street:  request.body.street,
                streetNumber:  request.body.streetNumber,
                inputName:  request.body.inputName,
                processedByUser: "",
                csResult:
                    {
                    email: request.body.email,
                    errorMessage: request.body.errorMessage,
                    chInformation: request.body.chInformation,
                    onlineScore: request.body.onlineScore,
                    totalScore: request.body.totalScore,
                    emailResultScore: request.body.emailResultScore,
                    networkResultScore: request.body.networkResultScore,
                    seResultScore: request.body.seResultScore,
                    professionalResultScore: request.body.professionalResultScore,
                    socialResultScore: request.body.socialResultScore,
                    emailRisk: request.body.emailRisk,
                    emailSeniority: request.body.emailSeniority,
                    emailUsage: request.body.emailUsage,
                    ntwNamingScore: request.body.ntwNamingScore,
                    ntwPresence: request.body.ntwPresence,
                    ntwGenderVerification: request.body.ntwGenderVerification,
                    seIndex: request.body.seIndex,
                    seAverageSalary: request.body.seAverageSalary,
                    seSqMeterPrice: request.body.seSqMeterPrice,
                    profRecommendations: request.body.profRecommendations,
                    profAvgTimeAtPosition: request.body.profAvgTimeAtPosition,
                    profPopularity: request.body.profPopularity,
                    socialActivity: request.body.socialActivity,
                    socialIQ: request.body.socialIQ,
                    socialTransparency: request.body.socialTransparency,
                    socialPopularity: request.body.socialPopularity
                }
        }]
    }));
    _req.end();
});

app.post('/createusagefile' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/createusagefile",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken,
        startDate: request.body.startDate,
        endDate: request.body.endDate
    }));
    _req.end();
});


app.post('/searchcities' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/searchcities",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken,
        countryCode : request.body.countryCode,
        cityName : request.body.cityName,
        streetName : request.body.streetName
    }));
    _req.end();
});
///////////////////////
app.post('/processbulk' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/processbulk",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken,
        fileName: request.body.fileName ,
        fileStream: request.body.fileStream
    }));
    _req.end();
});





app.post('/searchstreets' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "www.taryadigitalscore.com",
        "port": null,
        "path": "/api/tarya/searchstreets",
        "headers": {
            "content-type": "application/json"
        }
    };

    var _req = http.request(options, function (_res) {
        var chunks = [];

        _res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });

    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken,
        countryCode : request.body.countryCode,
        cityName : request.body.cityName,
        streetName : request.body.streetName
    }));
    _req.end();
});


app.listen(3000 , function () {
    console.log('Api working on 3000')
});